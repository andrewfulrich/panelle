/**
  If a line end is within snapping distance, snaps to that
  Else if a line is within snapping distance, snaps to that
  Else doesn't snap
**/
function snapToLines(lines,point,snappingDistance,intersections) {
  const closestEnd=lines
  .reduce((accum,l)=>[...accum,l[0],l[1]],[]) //reduce it to an array of coords, like the polyline coords
  .concat(intersections)
  .map(p=>({point:p,distance:Math.sqrt((p[0]-point[0])**2+(p[1]-point[1])**2)}))
  .filter(p=>p.distance < snappingDistance)
  .reduce((closest,current)=>current.distance<closest.distance ? current : closest,{point:[0,0],distance:Infinity})
  if(closestEnd.distance < Infinity) return closestEnd.point;

  //distance of point to line segement and projection of that point on the line
  //from https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
  function pointOnLine(x, y, x1, y1, x2, y2) {
    const dot = (x - x1) * (x2 - x1) + (y - y1) * (y2 - y1); //the dot product of line vector and a vector connecting line start with point
    const len_sq = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1); //length of line, squared
    let param = -1;
    if (len_sq != 0) //if line is length 0 just keep param=-1 so the point is the startpoint
        param = dot / len_sq;

    let xx, yy;

    if (param < 0) { //dot product is negative so angle is obtuse (i.e. outside of segment)
      xx = x1;
      yy = y1;
    } else if (param > 1) { //length of projection is greater than the line (i.e. outside of segment the other way)
      xx = x2;
      yy = y2;
    } else { //new vector is dotProduct(from_pt,from_to)/lengthSquared times original line vector, so its endpoint is the following:
      xx = x1 + param * (x2 - x1);
      yy = y1 + param * (y2 - y1);
    }
    const dx = x - xx;
    const dy = y - yy;
    return {
      point:[xx,yy],
      distance:Math.sqrt(dx * dx + dy * dy)
    }
  }

  const pointOnLineInfo =lines
    .map(l=>pointOnLine(point[0],point[1],l[0][0],l[0][1],l[1][0],l[1][1]))
    .reduce((closest,pOnLine,index)=>pOnLine.distance < closest.distance ? pOnLine : closest,{distance:Infinity})
  if(pointOnLineInfo.distance > snappingDistance) return point;
  
  return pointOnLineInfo.point
}

// line intercept math by Paul Bourke http://paulbourke.net/geometry/pointlineplane/
// Determine the intersection point of two line segments
// Return FALSE if the lines don't intersect
function intersect(x1, y1, x2, y2, x3, y3, x4, y4) {
  // Check if none of the lines are of length 0
  if ((x1 === x2 && y1 === y2) || (x3 === x4 && y3 === y4)) {
    return false
  }

  denominator = ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1))

  // Lines are parallel
  if (denominator === 0) {
    return false
  }

  let ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator
  let ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator

  // is the intersection along the segments
  if (ua < 0 || ua > 1 || ub < 0 || ub > 1) {
    return false
  }

  // Return an object with the x and y coordinates of the intersection
  let x = x1 + ua * (x2 - x1)
  let y = y1 + ua * (y2 - y1)

  return [x, y]
}
function findIntersections(lines) {
  if(lines.length<2) return []
  return lines.flatMap(
      (v, i) => lines.slice(i+1).map( w => intersect(w[0][0],w[0][1],w[1][0],w[1][1],v[0][0],v[0][1],v[1][0],v[1][1]) )
  ).filter(c=>c !== false);
}